

// Call `slide` to define a new slide.  `slide` takes a callback, which is used
// to set up the slide contents.  The first argument to the callback is `s`,
// the `Node` object representing the root of the slide.
slide((s) => {
    // Add new HTML elements to the slide by calling methods on `s`.  Here, we
    // call `h1` to create a new `<h1>` element.  The argument will be set as
    // the new element's `innerHTML`.
    let title = s.h1('Example Title Slide');

    // Methods that create new elements will return those elements wrapped up
    // in new `Node` objects.  We can use additional `Node` methods to
    // manipulate them.

    // For example, we can make the title text blue (using a
    // reference to the `--blue` CSS variable defined in `example-slides.css`).
    title.css('color', 'var(--blue)');

    // We can also center the text...
    title.css('text-align', 'center');

    // ...and place it in the middle of the slide.  The first argument to
    // `place` is the name of a point on the `Node`, and the second argument is
    // the position where that point should be located.  The `point` method
    // gives the current coordinates of a point on a `Node`, so the result of
    // this line is to move `title` so that its `center` point lines up with
    // the `center` of the entire slide.
    title.place('center', s.point('center'));


    // Now let's add a subtitle.  Since the argument to `div` is set as
    // `innerHTML`, we can include HTML tags in it.  Note the method chaining
    // as well - most `Node` methods that don't have anything useful to return,
    // such as `css`, // will instead return the `Node` itself.
    let subtitle =
        s.div('Read <code>example-slides.js</code> to see how it works!')
            .css('text-align', 'center');

    // Here we place the `north` point of `subtitle` 20 pixels below the
    // `south` point of `title`.  `xy` is the constructor for 2D vectors, and
    // `add` adds two vectors together.
    //
    // Note that the "20 pixel" measurement here is relative to the slide's
    // internal resolution, which is always 800x600 (the slide is then scaled
    // up to match the size of the browser window).  So "20 pixels" really
    // means "1/30 of the slide height".
    subtitle.place('north', title.point('south').add(xy(0, 20)));
});


// The `slide` callback can actually take a second argument, which is used to
// define animations.
slide((s, anim) => {
    // First, set up some non-animated elements.
    s.h1('Animation Example');

    s.div('Visible from the start');

    // Now we set up the first animation stage.  When the user advances from
    // the first stage of the slide to the second stage, the framework will run
    // the first `anim` callback.
    anim(() => {
        s.div('Visible after the first step');
    });

    // The `anim` method on `Node` objects is similar to `css`, but takes an
    // additional duration argument.  It performs an animated transition from
    // the old value to the new one over the given duration (in seconds).  We
    // can thus fade in an element by setting its `opacity` to zero, then
    // transitioning it to 1 with `anim`.
    anim(() => {
        s.div('This one fades in')
            .css('opacity', 0)
            .anim('opacity', 1, 1);
    });

    // The `fadeIn(duration)` method is shorthand for the two `opacity` changes
    // we just wrote.
    anim(() => {
        s.div('This one fades in too')
            .fadeIn(1);
    });

    // `anim` and `fadeIn` return promises, which resolve when the animation
    // ends.  We can then use normal promise methods to chain multi-step
    // animations.
    anim(() => {
        let d = s.div('Whoa, dude');
        d.fadeIn(1)
            .then(() => d.anim('color', 'var(--red)', 0.3))
            .then(() => d.anim('color', 'var(--orange)', 0.3))
            .then(() => d.anim('color', 'var(--yellow)', 0.3))
            .then(() => d.anim('color', 'var(--green)', 0.3))
            .then(() => d.anim('color', 'var(--cyan)', 0.3))
            .then(() => d.anim('color', 'var(--blue)', 0.3))
            .then(() => d.anim('color', 'var(--violet)', 0.3))
            .then(() => d.anim('color', 'var(--magenta)', 0.3))
            .then(() => d.anim('color', 'var(--white)', 0.3));
    });

    // The `delay` method just returns a promise that resolves after a given
    // time interval, without changing any properties.  This is useful if you
    // don't want to wait for an animation to completely finish before starting
    // the next one.
    anim(() => {
        s.div('One').fadeIn(5);
        s.delay(0.5).then(() => s.div('Two').fadeIn(5));
        s.delay(1.0).then(() => s.div('Three').fadeIn(5));
    });
});


slide((s, anim) => {
    s.h1('A Common Pitfall');

    s.div('Bad example:');

    // A common mistake is adding elements during animation steps in a way that
    // changes the layout of the enclosing container.  In this example,
    // previous elements will move at each animation step, as the longer items
    // cause the `<ul>` to grow wider.
    let ul1 = s.ul();
    ul1.li('First');
    anim(() => ul1.li('Second').fadeIn(1));
    anim(() => ul1.li('Third item is very long').fadeIn(1));


    let good = s.div('Good example');

    // The correct way to handle this is to create all the elements up-front,
    // but leave them hidden (`opacity: 0`) until the animation happens.
    let ul2 = s.ul();
    let first = ul2.li('First');
    let second = ul2.li('Second').css('opacity', 0);
    // `hide()` is an alias for `css('opacity', 0)`.
    let third = ul2.li('Third item is very long').hide();

    good.hide();
    ul2.hide();

    // Fade in the list, including the first list item.  Even though the third
    // item is not visible, the first item is already placed to accomodate it.
    anim(() => {
        good.fadeIn(1);
        ul2.fadeIn(1);
    });
    anim(() => second.fadeIn(1));
    anim(() => third.fadeIn(1));
});


slide((s, anim) => {
    // This slide shows an overview of this presentation's file structure,
    // drawn using the framework's SVG methods.
    s.h1('SVG Drawing');

    // Create an <svg> element with `svg(width, height)`.
    let svg = s.svg(750, 450);

    // SVG `Node`s provide the `htmlBox` method, which places some HTML content
    // and draws an SVG box around it.  The callback receives an empty SVG
    // <foreignObject> `Node`, and should populate it with HTML content.  Note
    // that we use `span` instead of `div` here - <div>s have `display: block`
    // by default, which makes them try to fill the entire width of their
    // parent.  This would make the SVG outline much larger than it needs to
    // be.
    let html = svg.htmlBox((x) => x.span('example-slides.html'));

    // Place the new box at the center-left of the SVG.  Passing a vector
    // instead of a name to `place` computes relative coordinates within the
    // parent `Node`.
    html.place('center', svg.point(xy(0.2, 0.5)));

    // An optional second argument to `htmlBox` allows setting the box size.
    // This is useful for drawing multiple related boxes with different (and
    // thus different-sized) text.  The HTML content will be centered inside
    // the fixed-size box.
    let css = svg.htmlBox((x) => x.span('example-slides.css'), xy(280, 50))
        .place('center', svg.point(xy(0.7, 0.1)));

    // `htmlBox` is actually the composition of two separate functions, `html`
    // and `box`.  `html` adds (and optionally centers) some HTML content
    // within an SVG, and `box` draws a box around some SVG content.
    let js = svg.box((x) => x.html((x) => x.span('example-slides.js'), xy(280, 50)))
        .place('center', svg.point(xy(0.7, 0.3)));

    // `box` (and the related method `boxRound`, which adds padding and a
    // rounded border) can also be used independently, to group together related
    // elements.
    let frame_css, frame_js;
    let framework = svg.boxRound((x) => {
        frame_css = x.htmlBox((x) => x.span('slides-framework.css'), xy(280, 50));
        frame_js = x.htmlBox((x) => x.span('slides-framework.js'), xy(280, 50));
        frame_js.place('center', frame_css.point('center').add(xy(0, 80)));
    }, 20).place('center', svg.point(xy(0.7, 0.8)));


    // The `arrow` method draws an arrow from one point to another.
    for (let dest of [css, js, frame_css, frame_js]) {
        svg.arrow(html.point('east'), dest.point('west'));
    }

    // Finally, label the `framework` box with a text element.
    s.span('Framework')
        .css('font-size', '75%')
        .place('north', framework.point('south').add(xy(0, 5)));
});


slide((s, anim) => {
    s.h1('Overlays and Callouts');

    // First, some text to work with.  We include <span> tags with some made-up
    // class names so that we can select specific pieces of text later on.
    let code = s.pre(`
def fib(n):
    if n < 2:
        return 1
    else:
        return <span class='rec'>fib(n - 1) + fib(n - 2)</span>

def fib2(n):
    a = 1
    b = 1
    for i in range(n):<span class='iter'></span>
        a, b <span class='assign'>=</span> b, a + b
    return a
    `);

    // Now, we want to annotate some parts of the text with callouts.  First,
    // we obtain the important elements using `select`, which finds the first
    // child of a `Node` that matches a CSS selector, and returns it as another
    // `Node`.
    let rec = code.select('.rec');
    let iter = code.select('.iter');
    let assign = code.select('.assign');

    // To actually draw the callouts, we use an "overlay", which is an <svg>
    // element that covers the entire slide.
    let svg = s.overlay();

    // Now we can create the callouts using the `htmlCallout` method.  The
    // first argument is a callback, as in `html`/`htmlBox`, and the second
    // indicates which side of the target element the callout will be on.
    // Callouts have a special named point called `tip`, which we use to place
    // the callout near the target element.
    anim(() => {
        svg.htmlCallout((x) => x.span('Two recursive calls - O(2<sup><i>n</i></sup>)'), 'above')
            .place('tip', rec.point('north'))
            .fadeIn(1);
    });

    anim(() => {
        svg.htmlCallout((x) => x.span('This loop is only O(<i>n</i>)'), 'right')
            .place('tip', iter.point('east'))
            .fadeIn(1);
    });

    // This text has two lines.  We use `display: inline-block` and
    // `text-align` to center the two lines relative to each other.
    anim(() => {
        svg.htmlCallout((x) => x.span('Simultaneous assignment avoids<br>the need for a temporary')
                .css('display', 'inline-block').css('text-align', 'center'), 'below')
            .place('tip', assign.point('south'))
            .fadeIn(1);
    });
});


slide((s, anim) => {
    s.h1('Importing External SVGs');

    // `inline` imports an SVG from a file and inlines its root <svg> node into
    // the current slide.  This means it's not just a static image - it's an
    // ordinary <svg> that we can manipulate using the existing `Node` methods.
    let shapes = s.inline('shapes.svg');

    // For example, we can grab layers out of the SVG and change their
    // visibility.  This `layer` method looks for an SVG <g> element with a
    // matching `inkscape:layer` attribute.
    shapes.layer('red').hide();
    shapes.layer('green').hide();

    anim(() => shapes.layer('red').fadeIn(1));
    anim(() => shapes.layer('green').fadeIn(1));

    // We can also adjust colors of drawing elements inside specific layers.
    anim(() => shapes.layer('blue').selectAll('path, rect') .forEach((x) => {
        x.anim('fill', 'var(--violet)', 1);
        x.anim('stroke', 'var(--magenta)', 1);
    }));
});

// One final note: the framework requires that any resource used with `inline`
// be preloaded.  This call to `preload` will ensure that the framework loads
// `shapes.svg` before starting the presentation, so that the future `inline`
// call has access to it.
preload('shapes.svg');
