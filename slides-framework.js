let HTML_NS = 'http://www.w3.org/1999/xhtml';
let SVG_NS = 'http://www.w3.org/2000/svg';

let WIDTH = 800;
let HEIGHT = 600;


function apply_attrs(elt, attrs) {
    attrs = attrs || {};
    for (let k of Object.getOwnPropertyNames(attrs)) {
        if (k.startsWith('css_')) {
            elt.style[k.substr(4)] = attrs[k];
        } else {
            elt.setAttribute(k, attrs[k]);
        }
    }
}

function mk_ns(ns, tag, text, attrs) {
    let elt = document.createElementNS(ns, tag);
    elt.innerHTML = text || '';
    apply_attrs(elt, attrs);
    return elt;
}

function mk(tag, text, attrs) {
    return mk_ns(HTML_NS, tag, text, attrs);
}

function mk_svg(tag, attrs) {
    return mk_ns(SVG_NS, tag, null, attrs);
}

function extend(a, b) {
    let result = {};
    if (a != null) {
        for (let k of Object.getOwnPropertyNames(a)) {
            result[k] = a[k];
        }
    }
    if (b != null) {
        for (let k of Object.getOwnPropertyNames(b)) {
            result[k] = b[k];
        }
    }
    return result;
}

function delay(s) {
    return new Promise(function(resolve, reject) {
        setTimeout(resolve, s * 1000);
    });
}

function frame() {
    return new Promise(function(resolve, reject) {
        window.requestAnimationFrame(resolve);
    });
}

function smart_split(s, delim) {
    if (s == '') {
        return [];
    } else {
        return s.split(delim);
    }
}

function clear_transition_time(elt, prop) {
    let props = smart_split(elt.style.transitionProperty, ',');
    let durs = smart_split(elt.style.transitionDuration, ',');

    for (let i = props.length - 1; i >= 0; --i) {
        if (props[i].trim() == prop) {
            props[i] = props[props.length - 1];
            props.pop();
            durs[i] = durs[durs.length - 1];
            durs.pop();
        }
    }

    elt.style.transitionProperty = props.join(',');
    elt.style.transitionDuration = durs.join(',');
}

function set_transition_time(elt, prop, dur) {
    let props = smart_split(elt.style.transitionProperty, ',');
    let durs = smart_split(elt.style.transitionDuration, ',');

    let found = false;

    for (let i = 0; i < props.length; ++i) {
        if (props[i].trim() == prop) {
            durs[i] = dur;
            found = true;
            break;
        }
    }

    if (!found) {
        props.push(prop);
        durs.push(dur);
    }

    elt.style.transitionProperty = props.join(',');
    elt.style.transitionDuration = durs.join(',');
}

function css_camel_prop(prop) {
    return prop.replace(/-([a-z])/, function(m, p1) {
        return p1.toUpperCase();
    });
}

function parse_hash(h) {
    if (!h.startsWith('#')) {
        return null;
    }

    let parts = h.substr(1).split(':');

    let idx;
    let stage;
    if (parts.length == 1) {
        idx = parts[0]|0;
        stage = 0;
    } else if (parts.length == 2) {
        idx = parts[0]|0;
        stage = parts[1]|0;
    } else {
        return null;
    }

    return { idx: idx, stage: stage };
}



//
// XY
//

function XY(x, y) {
    this.x = x;
    this.y = y;
}

function xy(x, y) {
    return new XY(x, y);
}

// Possible arguments:
//  add(xy)     - offset by xy.x, xy.y
//  add(x, y)   - offset by x, y
//  add(c)      - offset by c, c
XY.prototype.add = function(x, y) {
    if (x.constructor === XY) {
        return this.add(x.x, x.y);
    }
    if (y == null) {
        y = x;
    }
    return new XY(this.x + x, this.y + y);
};

XY.prototype.sub = function(x, y) {
    if (x.constructor === XY) {
        return this.sub(x.x, x.y);
    }
    if (y == null) {
        y = x;
    }
    return new XY(this.x - x, this.y - y);
};

XY.prototype.mul = function(c) {
    return new XY(this.x * c, this.y * c);
};

XY.prototype.div = function(c) {
    return new XY(this.x / c, this.y / c);
};

XY.prototype.midpoint = function(x, y) {
    return this.add(x, y).div(2);
};

XY.prototype.c = function() {
    return ' ' + this.x + ',' + this.y;
};

XY.prototype.toString = function() {
    return this.x + ',' + this.y;
};



// Thin wrapper around getBoundingClientRect
function get_rect_raw(elt) {
    var raw = elt.getBoundingClientRect();
    var min = new XY(raw.left, raw.top);
    var max = new XY(raw.right, raw.bottom);
    var size = max.sub(min);
    return {min: min, max: max, size: size};
}

// Find the slide containing `elt`.
function find_slide(elt) {
    while (elt != null && !elt.classList.contains('slide')) {
        elt = elt.parentElement;
    }
    return elt;
}

function relative_point(p, base, size) {
    return new XY(
            ((p.x - base.min.x) / base.size.x * size.x)|0,
            ((p.y - base.min.y) / base.size.y * size.y)|0
            );
}

function relative_rect(rect, base, size) {
    var min = relative_point(rect.min, base, size);
    var max = relative_point(rect.max, base, size);
    var size = max.sub(min);

    return {min: min, max: max, size: size};
}

// Get the absolute position of `elt` in slide coordinates.
function get_rect(elt) {
    let elt_rect = get_rect_raw(elt);
    let slide_rect = get_rect_raw(find_slide(elt));
    // Rescale, mapping `slide_rect` to (0, 0, 800, 600).
    return relative_rect(elt_rect, slide_rect, xy(WIDTH, HEIGHT));
}



function AnimContext() {
    this.instant = false;

    this.pending = 0;
    this.promise = null;
    this.resolve = null;
}


function InstantPromise() {
}

InstantPromise.prototype.then = function(f) {
    f();
    return this;
};



function Node(elt, anim_context) {
    this.elt = elt;
    this.anim_context = anim_context || new AnimContext();
    this.children = [];
    this.points = {};
}

Node.prototype._addChild = function(elt) {
    let n = new Node(elt, this.anim_context);
    this.elt.appendChild(n.elt);
    this.children.push(n);
    return n;
};

Node.prototype._addNodeChild = function(n) {
    this.elt.appendChild(n.elt);
    this.children.push(n);
    return n;
};

Node.prototype._insertNodeChild = function(n) {
    this.elt.insertBefore(n.elt, this.elt.firstChild);
    this.children.push(n);
    return n;
};

Node.prototype.select = function(sel) {
    let elt = this.elt.querySelector(sel);
    if (elt == null) {
        return null;
    }
    return new Node(elt, this.anim_context);
};

Node.prototype.selectAll = function(sel) {
    let elts = this.elt.querySelectorAll(sel);
    let nodes = [];
    for (let elt of elts) {
        nodes.push(new Node(elt, this.anim_context));
    }
    return nodes;
};

Node.prototype.layer = function(name) {
    return this.select('g[*|label=' + name + ']');
};

Node.prototype.note = function(content) {
    // TODO
};

Node.prototype.h1 = function(content) {
    return this._addChild(mk('h1', content));
};

Node.prototype.h2 = function(content) {
    return this._addChild(mk('h2', content));
};

Node.prototype.h3 = function(content) {
    return this._addChild(mk('h3', content));
};

Node.prototype.p = function(content) {
    return this._addChild(mk('p', content));
};

Node.prototype.span = function(content, attrs) {
    return this._addChild(mk('span', content, attrs));
};

Node.prototype.todo = function(content) {
    return this._addChild(mk('span', '[' + content + ']', {'css_color': 'var(--red)'}));
};

Node.prototype.code = function(content, attrs) {
    return this._addChild(mk('code', content, attrs));
};

Node.prototype.pre = function(content, attrs) {
    return this._addChild(mk('pre', content, attrs));
};

Node.prototype.div = function(content, attrs) {
    return this._addChild(mk('div', content, attrs));
};

Node.prototype.br = function() {
    return this._addChild(mk('br'));
};

Node.prototype.ul = function() {
    return this._addChild(mk('ul'));
};

Node.prototype.ol = function() {
    return this._addChild(mk('ol'));
};

Node.prototype.li = function(content) {
    return this._addChild(mk('li', content));
};

Node.prototype.img = function(src, attrs) {
    return this._addChild(mk('img', null, extend({
        'src': src,
    }, attrs)));
};

Node.prototype.cls = function(cls) {
    this.elt.classList.add(cls);
    return this;
};

Node.prototype.inline = function(url) {
    let xml = PRELOADER.objs[url];

    let content_type = 'image/svg+xml';

    let doc = new DOMParser().parseFromString(xml, content_type);
    return this._addChild(doc.documentElement);
};

Node.prototype.svg = function(w, h, attrs) {
    return this._addChild(mk_svg('svg', extend({
        'width': w,
        'height': h,
        'xmlns': SVG_NS,
        'xmlns:html': HTML_NS,
    }, attrs)));
};

Node.prototype.overlay = function() {
    let size = this.size();
    return this.svg(size.x, size.y, {
        css_position: 'absolute',
        css_top: '0',
        css_left: '0',
        css_zOrder: '1',
    });
};

Node.prototype.g = function(attrs) {
    return this._addChild(mk_svg('g', attrs));
};

Node.prototype.path = function(d, attrs) {
    return this._addChild(mk_svg('path', extend({
        'd': d,
        'css_stroke': 'var(--foreground)',
        'css_fill': 'none',
    }, attrs)));
};

Node.prototype.foreignObject = function(x, y, w, h, attrs) {
    return this._addChild(mk_svg('foreignObject', extend({
        'x': x,
        'y': y,
        'width': w,
        'height': h,
    }, attrs)));
};

Node.prototype.centered = function(f, size) {
    let g = this.g();
    let inner = g.g();
    f(inner);

    // Get the size of the inner group's contents
    let base = inner.point('origin');
    let max = xy(base.x, base.y)
    for (let child of inner.children) {
        let r = child.getRect();
        if (r.max.x > max.x) {
            max.x = r.max.x;
        }
        if (r.max.y > max.y) {
            max.y = r.max.y;
        }
    }
    let measured_size = max.sub(base);

    // Set the bounding box of the outer `g` using a dummy foreignObject.
    g.foreignObject(0, 0, size.x, size.y);
    // Center the main foreignObject within `g`.
    inner.elt.setAttribute('transform',
        `translate(${(size.x - measured_size.x) / 2},
                   ${(size.y - measured_size.y) / 2})`);

    return g;
};

Node.prototype.html = function(f, size) {
    if (size == null) {
        let foreign = this.foreignObject(0, 0, 1000, 1000);
        f(foreign);

        // Get the size of the foreignObject's contents
        let base = foreign.point('origin');
        let max = xy(base.x, base.y)
        for (let child of foreign.children) {
            let r = child.getRect();
            if (r.max.x > max.x) {
                max.x = r.max.x;
            }
            if (r.max.y > max.y) {
                max.y = r.max.y;
            }
        }
        let measured_size = max.sub(base).add(xy(1, 1));

        foreign.elt.setAttribute('width', measured_size.x);
        foreign.elt.setAttribute('height', measured_size.y);

        return foreign;
    } else {
        return this.centered((x) => x.html(f, null), size);
    }
};

Node.prototype.box = function(f, edit_shape) {
    let g = this.g();
    g.shape = g.path('', {
        'css_fill': 'var(--background)',
    });
    // This is required or else the text doesn't appear inside the box.  WTF??
    f(g);

    let origin = g.point('origin');
    let size = g.size();
    g.shape.elt.setAttribute('d', `M0,0 L0,${size.y} ${size.x},${size.y} ${size.x},0 Z`);

    if (edit_shape != null) {
        edit_shape(g.shape);
    }

    // Place points on the border of the content, not the border of the outer
    // shape.
    let offset = origin.sub(g.point('origin'));
    for (let k of Object.getOwnPropertyNames(DEFAULT_POINT_REL)) {
        let rel = DEFAULT_POINT_REL[k];
        g.points[k] = xy(size.x * rel.x, size.y * rel.y).add(offset);
    }

    return g;
};

Node.prototype.htmlBox = function(f, size) {
    return this.box((x) => x.html(f, size));
};

Node.prototype.boxRound = function(f, radius) {
    let g = this.g();
    g.shape = g.path('', {
        'css_fill': 'var(--background)',
    });
    f(g);

    let origin = g.point('origin');
    let size = g.size();
    let [w, h, r] = [size.x, size.y, radius];
    let d = `
        M 0,${-r}
        L ${w},${-r}
        A ${r},${r} 0 0 1 ${w+r},0
        L ${w+r},${h}
        A ${r},${r} 0 0 1 ${w},${h+r}
        L 0,${h+r}
        A ${r},${r} 0 0 1 ${-r},${h}
        L ${-r},0
        A ${r},${r} 0 0 1 0,${-r}
        Z`;
    g.shape.elt.setAttribute('d', d);

    return g;
};

Node.prototype.htmlBoxRound = function(f, radius, size) {
    return this.boxRound((x) => x.html(f, size), radius);
};

function callout_shape(w, h, r, side, cw, ch) {
    function c(x, y) {
        return ' ' + x + ',' + y;
    }

    // Build the path
    var k = 0.55228;

    var lines = [
        'l' + c(w, 0),
        'l' + c(0, h),
        'l' + c(-w, 0),
        'l' + c(0, -h),
    ];
    var corners = [
        'c' + c(r * k, 0) + c(r, r * (1 - k)) + c(r, r),
        'c' + c(0, r * k) + c(-r * (1 - k), r) + c(-r, r),
        'c' + c(-r * k, 0) + c(-r, -r * (1 - k)) + c(-r, -r),
        'c' + c(0, -r * k) + c(r * (1 - k), -r) + c(r, -r),
    ];

    switch (side) {
        case 'above':
            lines[2] = 'l' +
                c(-(w - cw) / 2, 0) +
                c(-cw / 2, ch) +
                c(-cw / 2, -ch) +
                c(-(w - cw) / 2, 0);
            break;
        case 'below':
            lines[0] = 'l' +
                c((w - cw) / 2, 0) +
                c(cw / 2, -ch) +
                c(cw / 2, ch) +
                c((w - cw) / 2, 0);
            break;

        case 'right':
            lines[3] = 'l' +
                c(0, -(h - cw) / 2) +
                c(-ch, -cw / 2) +
                c(ch, -cw / 2) +
                c(0, -(h - cw) / 2);
            break;
    }

    return 'M' + c(0, -r) +
        lines[0] + corners[0] +
        lines[1] + corners[1] +
        lines[2] + corners[2] +
        lines[3] + corners[3] +
        'Z';
}

Node.prototype.callout = function(f, side) {
    let g = this.g();
    g.shape = g.path('', {
        'css_fill': 'var(--background)',
    });
    // This is required or else the text doesn't appear inside the box.  WTF??
    f(g);

    let origin = g.point('origin');
    let size = g.size();
    let d = callout_shape(size.x, size.y, 10, side, 15, 30);
    g.shape.elt.setAttribute('d', d);

    // Place points on the border of the outline, not the bounding box
    // (including pointer).
    let offset = origin.sub(g.point('origin'));
    for (let k of Object.getOwnPropertyNames(DEFAULT_POINT_REL)) {
        let rel = DEFAULT_POINT_REL[k];
        let px = 5 * (rel.x * 2 - 1);
        let py = 5 * (rel.y * 2 - 1);
        g.points[k] = xy(size.x * rel.x + px, size.y * rel.y + py).add(offset);
    }
    let raw_size = g.size();
    switch (side) {
        case 'above':
            g.points['tip'] = xy(raw_size.x / 2, raw_size.y);
            break;
        case 'below':
            g.points['tip'] = xy(raw_size.x / 2, 0);
            break;
        case 'left':
            g.points['tip'] = xy(raw_size.x, raw_size.y / 2);
            break;
        case 'right':
            g.points['tip'] = xy(0, raw_size.y / 2);
            break;
    }

    return g;
};

Node.prototype.htmlCallout = function(f, side, size) {
    return this.callout((x) => x.html(f, size), side);
};

var UNIQUE_ID_COUNTER = 0;

Node.prototype.findDefId = function(name) {
    let svg = this.elt;
    while (svg.tagName.toLowerCase() != 'svg') {
        svg = svg.parentElement;
    }

    let defs_list = svg.getElementsByTagName('defs');
    for (let defs of defs_list) {
        for (let child = defs.firstChild; child != null; child = child.nextSibling) {
            if (child.getAttribute && child.getAttribute('name') == name) {
                let id = child.getAttribute('id');
                if (id == null) {
                    id = `def_${name}_${UNIQUE_ID_COUNTER}`;
                    child.setAttribute('id', id);
                    ++UNIQUE_ID_COUNTER;
                }
                return id;
            }
        }
    }

    // Not found.  Copy from the root.

    let root_defs = document.getElementById('svg-defs');
    let the_def = null;
    for (let child = root_defs.firstChild; child != null; child = child.nextSibling) {
        if (child.getAttribute && child.getAttribute('name') == name) {
            the_def = child.cloneNode(true);
            break;
        }
    }

    let id = `def_${name}_${UNIQUE_ID_COUNTER}`;
    ++UNIQUE_ID_COUNTER;
    the_def.setAttribute('id', id);

    if (defs_list.length > 0) {
        defs_list[0].appendChild(the_def);
    } else {
        let defs = mk_svg('defs');
        defs.appendChild(the_def);
        svg.appendChild(defs);
    }
    return id;
};

Node.prototype.poly = function(ps, close, attrs) {
    let d = `M ${ps[0].x}, ${ps[0].y}`;
    for (let p of ps.slice(1)) {
        d += `L ${p.x}, ${p.y}`;
    }
    if (close) {
        d += 'Z';
    }

    let path = mk_svg('path', extend({
        'd': d,
        'css_stroke': 'var(--foreground)',
        'css_fill': 'none',
    }, attrs));
    return this._addChild(path);
};

Node.prototype.line = function(p1, p2) {
    let base = this.point('origin');

    p1 = p1.sub(base);
    p2 = p2.sub(base);
    let mid = p1.add(p2).div(2);

    let d = `M ${p1.x},${p1.y} L ${p2.x},${p2.y}`;
    let path = mk_svg('path', {
        'd': d,
        'css_stroke': 'var(--foreground)',
        'css_fill': 'none',
    });
    let n = this._addChild(path);

    let r = n.getRect();
    let off = base.sub(r.min);
    n.points['start'] = p1.add(off);
    n.points['end'] = p2.add(off);
    n.points['midpoint'] = mid.add(off);

    return n;
};

Node.prototype.arrow = function(p1, p2) {
    return this.arrowCurved(p1, p2, xy(0, 0));
};

Node.prototype.arrowCurved = function(p1, p2, offset) {
    let base = this.point('origin');

    p1 = p1.sub(base);
    p2 = p2.sub(base);
    let real_mid = p1.add(p2).div(2);
    let mid = real_mid.add(offset);
    let c1 = p1.add(real_mid).div(2).add(offset);
    let c2 = p2.add(real_mid).div(2).add(offset);

    let d = `M ${p1.x},${p1.y}
             Q ${c1.x},${c1.y} ${mid.x},${mid.y}
             Q ${c2.x},${c2.y} ${p2.x},${p2.y}`;
    let path = mk_svg('path', {
        'd': d,
        'css_stroke': 'var(--foreground)',
        'css_fill': 'none',
        'css_markerEnd': `url(#${this.findDefId('arrow-large')})`,
        'css_zOrder': -1,
    });
    let n = this._addChild(path);

    let r = n.getRect();
    let off = base.sub(r.min);
    n.points['start'] = p1.add(off);
    n.points['end'] = p2.add(off);
    n.points['midpoint'] = mid.add(off);

    return n;
};

Node.prototype.attr = function(prop, value) {
    if (value == null) {
        this.elt.removeAttribute(prop);
    } else {
        this.elt.setAttribute(prop, value);
    }
    return this;
};

Node.prototype.css = function(prop, value) {
    this.elt.style[css_camel_prop(prop)] = value;
    return this;
};

Node.prototype.hide = function() {
    return this.css('opacity', 0);
};

Node.prototype._animProperty = function(prop, from_val, to_val, dur) {
    if (this.anim_context.instant) {
        this.elt.style[css_camel_prop(prop)] = to_val;
        return new InstantPromise();
    } else {
        clear_transition_time(this.elt, prop);
        this.elt.style[css_camel_prop(prop)] = from_val;
        return frame().then(() => {
            set_transition_time(this.elt, prop, dur + 's')
            this.elt.style[css_camel_prop(prop)] = to_val;
        }).then(() => delay(dur));
    }
};

Node.prototype.fadeIn = function(dur) {
    return this._animProperty('opacity', 0, 1, dur);
};

Node.prototype.delay = function(dur) {
    if (this.anim_context.instant) {
        return new InstantPromise();
    } else {
        return delay(dur);
    }
};

Node.prototype.frame = function() {
    if (this.anim_context.instant) {
        return new InstantPromise();
    } else {
        return frame();
    }
};

Node.prototype.anim = function(prop, to_val, dur) {
    let from_val = this.elt.style[css_camel_prop(prop)];
    return this._animProperty(prop, from_val, to_val, dur);
};

const DEFAULT_POINT_REL = {
    'origin':       xy(0.0, 0.0),
    'northwest':    xy(0.0, 0.0),
    'north':        xy(0.5, 0.0),
    'northeast':    xy(1.0, 0.0),
    'west':         xy(0.0, 0.5),
    'center':       xy(0.5, 0.5),
    'east':         xy(1.0, 0.5),
    'southwest':    xy(0.0, 1.0),
    'south':        xy(0.5, 1.0),
    'southeast':    xy(1.0, 1.0),
};

Node.prototype.getRect = function() {
    return get_rect(this.elt);
};

Node.prototype.point = function(name) {
    let r = this.getRect();

    let rel = null;
    if (name == null) {
        rel = DEFAULT_POINT_REL['center'];
    } else if (typeof name === 'object' && name.constructor === XY) {
        rel = name;
    } else if (this.points[name] == null) {
        rel = DEFAULT_POINT_REL[name];
    }
    if (rel != null) {
        return xy(r.min.x * (1 - rel.x) + r.max.x * rel.x,
                  r.min.y * (1 - rel.y) + r.max.y * rel.y);
    }

    // If all `rel` cases failed, then we know points[name] is non-null.
    return r.min.add(this.points[name]);
};

Node.prototype.place = function(name, dest) {
    if (this.elt.parentElement.namespaceURI == HTML_NS) {
        this.elt.style.position = 'absolute';
        this.elt.style.left = '0px';
        this.elt.style.top = '0px';
        this.elt.style.right = '';
        this.elt.style.bottom = '';

        let cur = this.point(name);
        let offset = dest.sub(cur);
        this.elt.style.left = offset.x + 'px';
        this.elt.style.top = offset.y + 'px';
    } else {
        let cur = this.point(name);
        let offset = dest.sub(cur);

        // NB: assumes no rotation/scaling/shearing of parent elements
        let old_transform = this.elt.getAttribute('transform') || '';
        this.elt.setAttribute('transform',
            old_transform + ` translate(${offset.x}, ${offset.y})`);
    }
    return this;
};

Node.prototype.size = function(name) {
    let r = get_rect(this.elt);
    return r.size;
};



function count_stages(f) {
    let n = new Node(mk('div', '', {'class': 'slide'}));
    let count = 1;
    f(n, () => { ++count; });
    return count;
}

function Slide(f) {
    this.f = f;
    this.num_stages = null;
}

Slide.prototype.init = function() {
    this.num_stages = count_stages(this.f);
};

Slide.prototype.build = function(stage, root, instant) {
    instant = instant || false;
    this.f(root, (a) => {
        if (stage > 0) {
            root.anim_context.instant = (stage != 1) || instant;
            if (root.anim_context.instant) {
                a();
            } else {
                frame().then(() => a());
            }
            --stage;
        }
    });
};



function Deck(root) {
    this.root = root;
    this.cur_slide = null;
    this.cur_slide_node = null;
    this.slides = [];
    this.idx = 0;
    this.stage = 0;

    this.number = mk('div', '0', {
        css_position: 'absolute',
        css_zIndex: '1000',
        css_right: '0.5em',
        css_top: '580px',
        css_fontSize: '50%',
        css_color: 'var(--foreground)',
    });
    this.number.classList.add('slide-number');
    this.root.appendChild(this.number);
}

Deck.prototype.slide = function(s) {
    this.slides.push(new Slide(s));
};

Deck.prototype.init = function() {
    for (let s of this.slides) {
        s.init();
    }
};

Deck.prototype.next = function() {
    if (this.stage < this.slides[this.idx].num_stages - 1) {
        ++this.stage;
        this.update();
    } else if (this.idx < this.slides.length - 1) {
        this.stage = 0;
        ++this.idx;
        this.update();
    } else {
        // Don't update.
        return;
    }
};

Deck.prototype.nextSlide = function() {
    if (this.idx < this.slides.length - 1) {
        this.stage = 0;
        ++this.idx;
        this.update();
    } else {
        // Don't update.
        return;
    }
};

Deck.prototype.prev = function() {
    if (this.stage > 0) {
        --this.stage;
        this.update(true);
    } else if (this.idx > 0) {
        --this.idx;
        this.stage = this.slides[this.idx].num_stages - 1;
        this.update(true);
    } else {
        // Don't update.
        return;
    }
};

Deck.prototype.prevSlide = function() {
    if (this.stage > 0) {
        this.stage = 0;
        this.update(true);
    } else if (this.idx > 0) {
        this.stage = 0;
        --this.idx;
        this.update(true);
    } else {
        // Don't update.
        return;
    }
};

Deck.prototype.update = function(instant) {
    if (this.cur_slide != null) {
        let old_slide = this.cur_slide;
        this.cur_slide = mk('div', '', {'class': 'slide'});
        this.root.replaceChild(this.cur_slide, old_slide);
    } else {
        this.cur_slide = mk('div', '', {'class': 'slide'});
        this.root.appendChild(this.cur_slide);
    }
    this.cur_slide_node = new Node(this.cur_slide);

    this.slides[this.idx].build(this.stage, this.cur_slide_node, instant);
    window.location.hash = '#' + this.idx + ':' + this.stage;
    this.number.textContent = (this.idx + 1) + '';
};

Deck.prototype.checkHash = function() {
    let h = window.location.hash;
    let p = parse_hash(h);
    if (p != null && (this.idx != p.idx || this.stage != p.stage)) {
        this.idx = p.idx;
        this.stage = p.stage;
        this.update(true);
    }
};


let DECK = new Deck(document.body);

function slide(f) {
    DECK.slide(f);
}



function Preloader() {
    this.objs = {};
    this.pending = 0;
    this.promise = Promise.resolve();
    this.resolve = null;
    this.errs = []
}

Preloader.prototype.preload = function(url) {
    let xhr = new XMLHttpRequest();
    xhr.open('GET', url);
    this._incPending();
    xhr.onload = () => {
        this.objs[url] = xhr.response;
        this._decPending();
    };
    xhr.onerror = (evt) => {
        this.errs.push('error loading ' + url + ': ' + evt.error);
        this._decPending();
    };
    xhr.send();
};

Preloader.prototype._incPending = function() {
    if (this.pending == 0) {
        this.promise = new Promise((resolve, reject) => {
            this.resolve = resolve;
        });
    }
    ++this.pending;
    console.log('inc', this.pending, this.promise, this.resolve);
};

Preloader.prototype._decPending = function() {
    console.log('dec', this.pending, this.promise, this.resolve);
    --this.pending;
    if (this.pending == 0) {
        if (this.resolve != null) {
            console.log('resolve promise');
            this.resolve();
            this.promise = Promise.resolve();
            this.resolve = null;
        }
    }
};

let PRELOADER = new Preloader();

function preload(url) {
    PRELOADER.preload(url);
}




function scale_body() {
    var w = window.innerWidth;
    var h = window.innerHeight;
    var wr = w / WIDTH;
    var hr = h / HEIGHT;
    var r = Math.min(wr, hr);
    var x_off = (window.innerWidth - WIDTH * r) / 2;
    var y_off = (window.innerHeight - HEIGHT * r) / 2;
    var xform =
        'translate(' + x_off + 'px, ' + y_off + 'px) ' +
        'scale(' + r + ')';
    document.body.style.transform = xform;
}
window.addEventListener('resize', scale_body);

scale_body();


document.addEventListener('DOMContentLoaded', function() {
    PRELOADER.promise.then(() => {
        console.log('finished preloading; errs: ', PRELOADER.errs);

        DECK.init();

        let p = parse_hash(window.location.hash);
        if (p != null) {
            DECK.idx = p.idx;
            DECK.stage = p.stage;
        }
        DECK.update(true);
    });
});

document.addEventListener('keydown', function(evt) {
    var handled = false;
    switch (evt.keyCode) {
        case 32:    // space
        case 39:    // right arrow
        case 34:    // page down
            if (!evt.shiftKey) {
                DECK.next();
            } else {
                DECK.nextSlide();
            }
            handled = true;
            break;
        case 37:    // left arrow
        case 33:    // page up
            if (!evt.shiftKey) {
                DECK.prev();
            } else {
                DECK.prevSlide();
            }
            handled = true;
            break;
    }

    if (handled) {
        evt.preventDefault();
        evt.stopPropagation();
    }
});

window.addEventListener('hashchange', function() {
    DECK.checkHash();
});
